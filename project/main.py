import pygame

# import system module to exec system commands
# Here we use this modulw to exit when we call pygame.QUIT
# so we don't get any errors
from sys import exit


def display_score():
    current_time = int(pygame.time.get_ticks()/1000) - start_time
    score_surface = test_font.render(
        f'Score:{current_time}', False, (64, 64, 64))
    score_rectangle = score_surface.get_rect(center=(400, 50))
    screen.blit(score_surface, score_rectangle)


# Starts pygame
pygame.init()

# Display surface stored in variable "screen" with ((width,height))
screen = pygame.display.set_mode((800, 400))

# This command is used to provide the title to the game window
pygame.display.set_caption("test_game")

# clock object == helps with time and helps to control the frame rate
clock = pygame.time.Clock()

# font is param/config is stored in the test variable here
# command pygame.font.Font(font_type, font_size) is used here to draw a font
test_font = pygame.font.Font("font/Pixeltype.ttf", 50)

game_active = True
start_time = 0

# test surface is display over the main or display surface
# Here we have created a surface using pygame.Surface((w,h))
# here we have stored it in test_surface with ((width,height))
# test_surface = pygame.Surface((100, 200))
# test_surface.fill("Red")  # Gave test surface Red color

# Intro screen
player_stand = pygame.image.load(
    'graphics/Player/player_stand.png').convert_alpha()
player_stand_rectangle = player_stand.get_rect(center=(400, 200))
# sky_surface(Surface 1)
# .convert() convert the image to a more optimal format for pygame
sky_surface = pygame.image.load("graphics/Sky.png").convert()

# ground_surface (Surface 2)
ground_surface = pygame.image.load("graphics/ground.png").convert()

# score_surface                 ("The font", Anti-aliasing{boolean},"color")
# score_surface = test_font.render("My Game", False, "Black").convert()

# score_rectangle
# score_rectangle = score_surface.get_rect(center=(400, 50))

""" snail_surface (Surface 4)
.convert_alpha() converts to optimal and removes bg/adjusts transparency """
snail_surface = pygame.image.load("graphics/snail/snail1.png").convert_alpha()

# snail rectangle
snail_rectangle = snail_surface.get_rect(bottomright=(600, 300))

# player_surface
player_surface = pygame.image.load(
    "graphics/Player/player_walk_1.png").convert_alpha()

"""rectangles are used to define proper dimensions of and
image used with pygame.
With reactangle's proper dimensions it can be used to define collusion boxes
and various other dimensions properly
rectangle command pygame.Rect(left,top,width,height
To use exact same size as surface player_surface.get_rect(grab_point = (x,y))
grab_point is the point on the rectangle edge
{eg: topleft is topleft vertex, topmid is the midpoint on top edge}"""

""" player_rectangle grabbed mid of the bottom edge and
placed it at x=80 and y=300 """
player_rectangle = player_surface.get_rect(midbottom=(80, 300))

player_gravity = -15

while True:
    # Event loop (Makes the game window persistent)
    for event in pygame.event.get():  # pygame.Event.get() to get events
        if event.type == pygame.QUIT:  # pygame.QUIT == close "X" button
            pygame.quit()
            exit()  # To kill the loops an consequently the window as well

        if game_active:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and player_rectangle.bottom >= 300:
                    player_gravity = -20
        else:
            if event.type == pygame.KEYDOWN and pygame.K_SPACE:
                game_active = True
                snail_rectangle.left = 800
                start_time = int(pygame.time.get_ticks()/1000)

    """ # Example code for surface
     attached the test_surface to display_surface using blit
     blit stands for "(block image transfer)" block doesn't mean stop here
     here the test_surface is place at (x, y) coordinates
     screen.blit(test_surface(0, 0))"""

    if game_active:
        # sky_surface placed (Surface 1)
        screen.blit(sky_surface, (0, 0))

        # ground_surface placed (Surface 2)
        screen.blit(ground_surface, (0, 300))

        # pygame.draw.rect(screen, '#c0e8ec', score_rectangle)
        # pygame.draw.rect(screen, '#c0e8ec', score_rectangle, 10)

        # # score_surface placed (Surface 3)
        # screen.blit(score_surface, score_rectangle)
        display_score()

        """ # Previous for snail animation code
        snail_x_pos -= 4  # update the snail's x position in the loop
        if snail_x_pos < -100:  # if snail is at coordinate 100
        snail_x_pos = 800  # position will be reset to 800 """

        snail_rectangle.x -= 5  # moving x axis in -1 direction(left)
        if snail_rectangle.right <= 0:  # when right edge of rectangle goes to 0
            snail_rectangle.left = 800

        # snail_surface placed (Surface 4)
        screen.blit(snail_surface, snail_rectangle)

        # player_surface placed (Surface 5)
        player_gravity += 1
        player_rectangle.y += player_gravity
        if player_rectangle.bottom >= 300:
            player_rectangle.bottom = 300
        screen.blit(player_surface, player_rectangle)

        """
       rectangle1.colliderect(rectangle2) is used to
       detect collusion betwixt 2 rectangles
       rectangle1 and rectangle2 being the arguments here
           """
        # collusion
        if snail_rectangle.colliderect(player_rectangle):
            game_active = False
    else:
        screen.fill((94, 129, 162))
        screen.blit(player_stand, player_stand_rectangle)

    # keys = pygame.key.get_pressed()
    # if keys[pygame.K_SPACE]:
    #     print('jump')

    # if player_rectangle.colliderect(snail_rectangle):
    # print("collusion")

    """ draw all our elements
    update everything """
    pygame.display.update()

    # Makes sure the while loop doesn't loop anymore than 60 times a second
    clock.tick(60)
